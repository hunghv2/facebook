﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Model
{
    public class Comment
    {
        public int CommentId { set; get; }
        public string CommentContent { set; get; }
        [ForeignKey("ParentComment")]
        public int? ParentCommentId { set; get; }
        public virtual Comment ParentComment { set; get;}
        public DateTime CommentTime { set; get;}
        public Reaction reaction { set; get; }
    }

}
