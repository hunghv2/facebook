﻿using System;

namespace Domain.Model
{
    public class Notification
    {
        public int NotificationId { get; set; }
        public string UserId { get; set; }
        public string NotificationContent { get; set; }
        public DateTime Time { get; set; }
    }
}
