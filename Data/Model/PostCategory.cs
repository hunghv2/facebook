﻿namespace Domain.Model
{
    public class PostCategory
    {
        public int PostCategoryId { set; get; }
        public string PostCategoryName { set; get; }
    }
}
