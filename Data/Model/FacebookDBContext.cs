﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Domain.Model
{
    public class FacebookDBContext : IdentityDbContext<UserInfo>
    {
        public FacebookDBContext(DbContextOptions<FacebookDBContext> options):base(options)
        {
            //options.UseSqlServer(@"Server=.;Database=FacebookDB;Trusted_Connection=True;MultipleActiveResultSets=true");
        }

        public FacebookDBContext() { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=.;Database=FacebookDB;Trusted_Connection=True;MultipleActiveResultSets=true");
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            // Bỏ tiền tố AspNet của các bảng trong Identity
            base.OnModelCreating(builder);
            foreach (var entityType in builder.Model.GetEntityTypes())
            {
                var tableName = entityType.GetTableName();
                if (tableName.StartsWith("AspNet"))
                {
                    entityType.SetTableName(tableName.Substring(6));
                }
            }
        }

        public DbSet<UserInfo> UserInfo { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<PostCategory> PostCategories { get; set; }
        public DbSet<Media> Medias { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Reaction> Reactions { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<UserRoleInfo> UserRoleInfos { get; set; }
    }
}
