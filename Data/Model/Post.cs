﻿using System;

namespace Domain.Model
{
    public class Post
    {
        public Guid PostId { set; get; }
        public string PostContent { set; get; }
        public DateTime PostTime { set; get; }
        public string UserId { get; set; }
        public int PostCategoryId { get; set; }
    }
}
