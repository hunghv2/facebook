﻿using static Common.Constant;

namespace Domain.Model
{
    public class Media
    {
        public int MediaId { get; set; }
        public string PostId { get; set; }
        public string FilePath { get; set; }
        public MediaType FileType { get; set; }
        public string UserId { get; set; }
    }
}
