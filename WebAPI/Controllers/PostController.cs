﻿using Common;
using Common.Helper;
using Dtos.ViewModel;
using Dtos.ViewRequest;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Service.Interface.IService;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using static Common.Constant;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    //[Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
    [ApiController]
    public class PostController : ControllerBase
    {
        private readonly IPostService _postService;
        private readonly IMediaService _mediaService;
        private readonly IReactionService _reactionService;
        private readonly IUserService _userHelper;
        public PostController(IPostService postService, IMediaService mediaService, IReactionService reactionService, IUserService userHelper)
        {
            _postService = postService;
            _mediaService = mediaService;
            _reactionService = reactionService;
            _userHelper = userHelper;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllPost()
        {
            List<PostVm> listPost = await _postService.GetAll();
            return Ok(ApiResponse<object>.Success(listPost));
        }

        [HttpPost("create")]
        public async Task<IActionResult> CreatePost([FromForm] PostRq post, List<IFormFile> postImage)
        {
            Guid postId = Guid.NewGuid();
            await _postService.CreatePost(postId, post);
            //Upload file
            foreach (var formFile in postImage)
            {
                if (formFile.Length > 0)
                {
                    Guid userId = Guid.Parse(_userHelper.GetUserId());
                    var path = UploadHelper.GetMediaPath(userId);
                    string fileName = await UploadHelper.UploadMedia(formFile, path);

                    //Them vao bang media
                    var extension = Path.GetExtension(fileName);
                    var fileType = extension switch
                    {
                        ".png" => MediaType.Image,
                        ".jpg" => MediaType.Image,
                        ".mp4" => MediaType.Video,
                        ".mpg" => MediaType.Video,
                        ".m4v" => MediaType.Video,
                        ".flv" => MediaType.Video,
                        _ => MediaType.Image
                    };           
                    MediaRq mediaRq = new MediaRq()
                    {
                        PostId = postId.ToString(),
                        FilePath = fileName,
                        FileType = fileType
                    };

                    await _mediaService.CreateMedia(mediaRq);
                }
            }

            var postVm = new
            {
                PostId = postId.ToString()
            };
            return Ok(ApiResponse<object>.Success(postVm));
        }

        [HttpGet("{postId}")]
        public async Task<IActionResult> GetPost(Guid postId)
        {
            PostVm post = await _postService.GetPostById(postId);
            if(post == null)
            {
                return NotFound(ApiResponse<object>.Fail("404","This post is not found"));
            }
            return Ok(ApiResponse<PostVm>.Success(post));
        }

        [HttpPost("react")]
        public async Task<IActionResult> ReactionPost(ReactionRq reactionRq)
        {
            await _reactionService.ReactionPost(reactionRq);
            return Ok(ApiResponse<object>.Success());
        }
    }
}
