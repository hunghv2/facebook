﻿using AutoMapper;
using Common;
using Dtos.ViewRequest;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Service.Interface.IService;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    //[Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private readonly ICommentService _commentService;
        private readonly IConfiguration _configuration;
        private readonly IMapper Mapper;
        public CommentController(IConfiguration configuration, IMapper mapper, ICommentService commentService)
        {
            _configuration = configuration;
            Mapper = mapper;
            _commentService = commentService;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetComment(int id)
        {
            var listComment = await _commentService.GetCommentAsync(id);
            return Ok(ApiResponse<object>.Success(listComment));
        }

        [HttpGet("{id}/child")]
        public async Task<IActionResult> GetChildComment(int id)
        {
            var listComment = await _commentService.GetChildComment(id);
            return Ok(ApiResponse<object>.Success(listComment));
        }

        [HttpPost("create")]
        public async Task<IActionResult> CreateComment([FromBody]CommentRq comment)
        {
            await _commentService.CreateComment(comment);
            return StatusCode(201);
        }
    }
}
