﻿using AutoMapper;
using Dtos.ViewRequest;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Service.Interface.IService;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentDetailController : ControllerBase
    {
        private readonly ICommentDetailService _commentDetailService;
        private readonly IConfiguration _configuration;
        private readonly IMapper Mapper;
        public CommentDetailController(IConfiguration configuration, IMapper mapper, ICommentDetailService commentDetailService)
        {
            _configuration = configuration;
            Mapper = mapper;
            _commentDetailService = commentDetailService;
        }

        [HttpPost("create")]
        public async Task<IActionResult> CreateCommentDetail(CommentDetailRq commentdetail)
        {
            await _commentDetailService.CreateCommentDetail(commentdetail);
            return StatusCode(201);
        }
    }
}
