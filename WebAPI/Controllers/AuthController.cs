﻿using AutoMapper;
using Common;
using Domain.Model;
using Dtos.ViewRequest.Auth;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly SignInManager<UserInfo> signInManager;
        private readonly UserManager<UserInfo> userManager;
        private readonly IMapper Mapper;
        public AuthController(SignInManager<UserInfo> signInManager, UserManager<UserInfo> userManager, IMapper mapper)
        {
            this.signInManager = signInManager;
            this.userManager = userManager;
            this.Mapper = mapper;
        }

        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> Register([FromBody] UserInfoRq userDetails)
        {
            if (!ModelState.IsValid || userDetails == null)
            {
                return BadRequest(ApiResponse<object>.Fail("User Registration Failed"));
            }

            var identityUser = Mapper.Map<UserInfo>(userDetails);
            var result = await userManager.CreateAsync(identityUser, userDetails.Password);
            if (!result.Succeeded)
            {
                var dictionary = new ModelStateDictionary();
                foreach (IdentityError error in result.Errors)
                {
                    dictionary.AddModelError(error.Code, error.Description);
                }

                return new BadRequestObjectResult(new {Status=Constant.ApiResponseFailed, Message = "User Registration Failed", Errors = dictionary });
            }

            return Ok(ApiResponse<object>.Success("User Reigstration Successful"));
        }

        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login([FromBody] LoginCredentials credentials)
        {
            if (!ModelState.IsValid || credentials == null)
            {
                return BadRequest(ApiResponse<object>.Fail("Login failed"));
            }

            var identityUser = await userManager.FindByNameAsync(credentials.Username);
            if (identityUser == null)
            {
                return BadRequest(ApiResponse<object>.Fail("Login failed"));
            }

            var result = userManager.PasswordHasher.VerifyHashedPassword(identityUser, identityUser.PasswordHash, credentials.Password);
            if (result == PasswordVerificationResult.Failed)
            {
                return BadRequest(ApiResponse<object>.Fail("Login failed"));
            }

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Email, identityUser.Email),
                new Claim(ClaimTypes.Name, $"{identityUser.FirstName} {identityUser.LastName}"),
                new Claim(ClaimTypes.NameIdentifier, identityUser.Id)
            };

            var claimsIdentity = new ClaimsIdentity(
                claims, CookieAuthenticationDefaults.AuthenticationScheme);

            await HttpContext.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(claimsIdentity));

            return Ok(ApiResponse<object>.Success("Logged in"));
        }

        [HttpPost]
        [Route("Logout")]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return Ok(ApiResponse<object>.Success(new { Token = "" }));
        }
    }
}
