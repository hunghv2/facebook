﻿using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Common.Helper
{
    public class UploadHelper
    {
        public static async Task<string> UploadMedia(IFormFile file, string path)
        {
            bool exists = Directory.Exists(path);
            if (!exists)
                Directory.CreateDirectory(path);
            var filePath = Path.Combine(path, file.FileName);
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            return file.FileName;
        }

        public static string GetMediaPath(Guid userId)
        {
            return Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", userId.ToString(), "media");
        }
    }
}
