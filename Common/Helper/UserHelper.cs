﻿using Microsoft.AspNetCore.Http;
using System;
using System.Security.Claims;
namespace Common.Helper
{
    public interface IUserService
    {
        string GetUserId();
        string GetUserEmail();
        string GetUserFullName();
    }

    public class UserHelper : IUserService
    {
        private readonly IHttpContextAccessor _context;

        public UserHelper(IHttpContextAccessor context)
        {
            _context = context;
        }

        public string GetUserId()
        {
            return Guid.NewGuid().ToString();
            //return _context.HttpContext.User?.FindFirst(ClaimTypes.NameIdentifier).Value;
        }

        public string GetUserEmail()
        {
            return _context.HttpContext.User?.FindFirst(ClaimTypes.Email).Value;
        }

        public string GetUserFullName()
        {
            return _context.HttpContext.User?.FindFirst(ClaimTypes.Name).Value;
        }
    }
}
