﻿namespace Common
{
    public static class Constant
    {
        public const string SecretKey = "ByYM000OLlMQG6VVVp1OH7Xzyr7gHuw1qvUC5dcGt3SNM";

        //role
        public const string RoleCustomer = "Customer";
        public const string RoleAdmin = "Admin";

        //Api response status 
        public const string ApiResponseFailed = "fail";
        public const string ApiResponseSuccess = "success";
        public enum MediaType
        {
            Image,
            Video,
            Avatar
        }
        public enum ReactionType
        {
            Like,
            Sad,
            Haha,
            Heart,
        };
    }
}
