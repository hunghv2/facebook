﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Common.Constant;

namespace Dtos.ViewModel
{
    public class ReactionVm
    {
        public int ReactionId { get; set; }
        public int? CommentId { get; set; }
        public Guid? PostId { get; set; }
        public ReactionType Reactions { get; set; }
        public string UserId { get; set; }
        public DateTime ReactionTime { get; set; }
    }
}
