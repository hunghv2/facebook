﻿using Domain.Model;
using System;

namespace Dtos.ViewModel
{
    public class CommentVm
    {
        public int CommentId { set; get; }
        public string CommentContent { set; get; }
        public int ParentCommentId { set; get; }
        public CommentVm ParentComment { set; get; }
        public DateTime CommentTime { set; get; }
        public Reaction reaction { set; get; }
    }
}
