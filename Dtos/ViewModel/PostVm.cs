﻿using System;

namespace Dtos.ViewModel
{
    public class PostVm
    {
        public string PostId { set; get; }
        public string PostContent { set; get; }
        public DateTime PostTime { set; get; }
        public string UserId { get; set; }
        public int PostCategoryId { get; set; }
    }
}
