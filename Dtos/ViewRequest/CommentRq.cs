﻿namespace Dtos.ViewRequest
{
    public class CommentRq
    {
        public string CommentContent { set; get; }
        public int? ParentCommentId { set; get; }
    }
}
