﻿namespace Dtos.ViewRequest
{
    public class PostRq
    {
        public string PostContent { set; get; }     
        public int PostCategoryId { get; set; }
    }
}
