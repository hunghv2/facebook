﻿using System;
using static Common.Constant;

namespace Dtos.ViewRequest
{
    public class ReactionRq
    {
        public int? CommentId { get; set; }
        public Guid? PostId { get; set; }
        public ReactionType Reactions { get; set; }
    }
}
