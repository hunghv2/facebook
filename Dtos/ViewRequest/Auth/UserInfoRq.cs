﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Dtos.ViewRequest.Auth
{
    public class UserInfoRq
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }      

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Phone { get; set; }

        [Required]
        public DateTime DOB { get; set; }

        public string Address { get; set; }
    }
}
