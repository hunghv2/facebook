﻿using AutoMapper;
using Domain.Model;
using Dtos.ViewRequest;
using Dtos.ViewRequest.Auth;

namespace Dtos.Mapping.ModelToRequestModel
{
    public class RequestModelToModel : Profile
    {
        public RequestModelToModel()
        {
            
            CreateMap<CommentRq, Comment>();
            CreateMap<NotificationRq, Notification>();
            CreateMap<PostRq, Post>();
            CreateMap<PostCategoryRq, PostCategory>();
            CreateMap<MediaRq, Media>();
            CreateMap<ReactionRq, Reaction>();
            CreateMap<UserInfoRq, UserInfo>().ForMember(
                des => des.UserName, 
                opt => opt.MapFrom(src => src.Email)
            );
        }
    }
}
