﻿using AutoMapper;
using Domain.Model;
using Dtos.ViewModel;

namespace Dtos.Mapping.ModelToViewModel
{
    public class ModelToViewModel : Profile
    {
        public ModelToViewModel()
        {
            CreateMap<Comment, CommentVm>();
            CreateMap<Notification, NotificationVm>();
            CreateMap<Post, PostVm>();
            CreateMap<PostCategory, PostCategoryVm>();
            CreateMap<Media, MediaVm>();
        }
    }
}
