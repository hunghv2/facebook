﻿using Domain.Model;
using Service.Interface.IRepository;

namespace Service.Repository
{
    public class MediaRepository : BaseRepository<Media>, IMediaRepository
    {
        private readonly FacebookDBContext _context;
        public MediaRepository(FacebookDBContext context) : base(context)
        {
            _context = context;
        }
    }
}
