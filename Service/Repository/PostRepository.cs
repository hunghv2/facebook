﻿using Domain.Model;
using Service.Interface.IRepository;

namespace Service.Repository
{
    public class PostRepository : BaseRepository<Post>, IPostRepository
    {
        private readonly FacebookDBContext _context;

        public PostRepository(FacebookDBContext context) : base(context)
        {
            _context = context;
        }
    }
}
