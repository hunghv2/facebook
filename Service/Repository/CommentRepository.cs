﻿using Domain.Model;
using Service.Interface.IRepository;

namespace Service.Repository
{
    public class CommentRepository : BaseRepository<Comment>, ICommentRepository
    {
        private readonly FacebookDBContext _context;

        public CommentRepository(FacebookDBContext context) : base(context)
        {
            _context = context;
        }
    }
}
