﻿using Domain.Model;
using Service.Interface.IRepository;

namespace Service.Repository
{
    public class ReactionRepository : BaseRepository<Reaction>, IReactionRepository
    {
        private readonly FacebookDBContext _context;

        public ReactionRepository(FacebookDBContext context) : base(context)
        {
            _context = context;
        }
    }
}
