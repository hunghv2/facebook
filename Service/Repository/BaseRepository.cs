﻿using Common.Exceptions;
using Domain.Model;
using Microsoft.EntityFrameworkCore;
using Service.Interface.IRepository;
using System.Linq;
using System.Threading.Tasks;

namespace Service.Repository
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        private readonly FacebookDBContext _context;
        internal DbSet<TEntity> dbSet;

        public BaseRepository(FacebookDBContext context)
        {
            _context = context;
            this.dbSet = _context.Set<TEntity>();
        }
        public virtual async Task Add(TEntity obj)
        {
            await _context.AddAsync(obj);
        }

        public virtual IQueryable<TEntity> GetAll() =>
            _context.Set<TEntity>();

        public virtual async Task<TEntity> GetById(int id)
        {
            return await _context.Set<TEntity>().FindAsync(id);
        }

        public virtual async Task<TEntity> GetById(string id)
        {
            return await _context.Set<TEntity>().FindAsync(id);
        }

        public virtual async Task Delete(TEntity obj)
        {
            _context.Set<TEntity>().Remove(obj);
        }

        public virtual async Task Delete(int id)
        {
            var obj = await _context.Set<TEntity>().FindAsync(id);
            if (obj == null)
            {
                throw new EntityNotFoundException("RESOURCE_NOT_FOUND");
            }
            _context.Set<TEntity>().Remove(obj);
        }

        public virtual async Task Update(TEntity obj)
        {
            _context.Entry(obj).State = EntityState.Modified;
        }

        public void Dispose() =>
            _context.Dispose();

    }
}
