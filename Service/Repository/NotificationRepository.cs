﻿using Domain.Model;
using Service.Interface.IRepository;

namespace Service.Repository
{
    public class NotificationRepository : BaseRepository<Notification>, INotificationRepository
    {
        private readonly FacebookDBContext _context;

        public NotificationRepository(FacebookDBContext context):base(context)
        {
            _context = context;
        }
    }
}
