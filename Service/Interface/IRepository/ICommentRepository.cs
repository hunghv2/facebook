﻿using Domain.Model;

namespace Service.Interface.IRepository
{
    public interface ICommentRepository : IBaseRepository<Comment>
    {
    }
}
