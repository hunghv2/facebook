﻿using Domain.Model;

namespace Service.Interface.IRepository
{
    public interface IMediaRepository : IBaseRepository<Media>
    {
    }
}
