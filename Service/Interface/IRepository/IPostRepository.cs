﻿using Domain.Model;

namespace Service.Interface.IRepository
{
    public interface IPostRepository : IBaseRepository<Post>
    {
    }
}
