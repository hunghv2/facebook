﻿using Domain.Model;

namespace Service.Interface.IRepository
{
    public interface IPostCategoryRepository : IBaseRepository<PostCategory>
    {
    }
}
