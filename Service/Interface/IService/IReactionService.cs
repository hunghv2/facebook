﻿using Dtos.ViewRequest;
using System.Threading.Tasks;

namespace Service.Interface.IService
{
    public interface IReactionService
    {
        Task ReactionPost(ReactionRq reactionRq);
    }
}
