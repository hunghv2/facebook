﻿using Dtos.ViewModel;
using Dtos.ViewRequest;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Service.Interface.IService
{
    public interface IPostService
    {
        Task<List<PostVm>> GetAll();
        Task<List<PostVm>> GetByUser(string id);
        Task<PostVm> GetPostById(Guid id);
        Task CreatePost(Guid postId, PostRq post);
        Task EditPost(PostRq post);
        Task DeletePost(int id);
    }
}
