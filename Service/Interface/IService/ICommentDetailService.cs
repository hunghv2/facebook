﻿using Dtos.ViewRequest;
using System.Threading.Tasks;

namespace Service.Interface.IService
{
    public interface ICommentDetailService
    {
        Task CreateCommentDetail(CommentDetailRq commentdetail);
        Task EditCommentDetail(CommentDetailRq commentdtail);
        Task DeleteCommentDetail(int id);
    }
}
