﻿using Dtos.ViewModel;
using Dtos.ViewRequest;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Service.Interface.IService
{
    public interface IPostCategoryService
    {
        Task<List<PostCategoryVm>> GetByUser(int id);
        Task<PostCategoryVm> GetPostCategoryById(int id);
        Task CreatePostCategory(PostCategoryRq post);
        Task EditPostCategory(PostCategoryRq post);
        Task DeletePostCategory(int id);
    }
}
