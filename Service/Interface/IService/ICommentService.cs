﻿using Dtos.ViewModel;
using Dtos.ViewRequest;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Service.Interface.IService
{
    public interface ICommentService
    {
        Task<List<CommentVm>> GetCommentAsync(int id);
        Task<List<CommentVm>> GetChildComment(int id);
        Task CreateComment(CommentRq comment);
        Task EditComment(CommentRq comment);
        Task DeleteComment(int id);
    }
}
