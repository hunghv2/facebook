﻿using Dtos.ViewModel;
using Dtos.ViewRequest;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Service.Interface.IService
{
    public interface IMediaService
    {
        Task<List<MediaVm>> GetByUser(int userId);
        Task<MediaVm> GetMediaById(int id);
        Task<MediaVm> GetMediaByPostId(int postId);
        Task CreateMedia(MediaRq media);
        Task EditMedia(MediaRq post);
        Task DeleteMedia(int id);
    }
}
