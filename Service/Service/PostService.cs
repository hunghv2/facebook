﻿using AutoMapper;
using Common.Helper;
using Domain.Model;
using Dtos.ViewModel;
using Dtos.ViewRequest;
using Microsoft.EntityFrameworkCore;
using Service.Interface.IService;
using Service.Interface.IUnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Service.Service
{
    public class PostService : BaseService, IPostService
    {
        private readonly IUserService UserHelper;

        public PostService(IMapper mapper, IUnitOfWork unitOfWork, IUserService userHelper)
        {
            Mapper = mapper;
            UnitOfWork = unitOfWork;
            UserHelper = userHelper;
        }

        public async Task<List<PostVm>> GetAll()
        {
            return await Mapper.ProjectTo<PostVm>(UnitOfWork.PostRepository.GetAll().OrderByDescending(x=>x.PostTime)).ToListAsync();
        }

        public async Task<PostVm> GetPostById(Guid id)
        {
            Post post = await UnitOfWork.PostRepository.GetAll().FirstOrDefaultAsync(x=>x.PostId == id);
            return Mapper.Map<PostVm>(post);
        }


        public async Task CreatePost(Guid postId, PostRq postRq)
        {
            Post post = Mapper.Map<Post>(postRq);
            post.PostId = postId;
            post.UserId = UserHelper.GetUserId();
            post.PostTime = DateTime.Now;
            await UnitOfWork.PostRepository.Add(post);
            await UnitOfWork.CompleteAsync();
        }

        public async Task DeletePost(int id)
        {
            await UnitOfWork.PostRepository.Delete(id);
            await UnitOfWork.CompleteAsync();
        }

        public async Task EditPost(PostRq postrq)
        {
            Post post = Mapper.Map<Post>(postrq);
            await UnitOfWork.PostRepository.Update(post);
            await UnitOfWork.CompleteAsync();
        }

        public Task<List<PostVm>> GetByUser(string id)
        {
            throw new NotImplementedException();
        }

    }
}
