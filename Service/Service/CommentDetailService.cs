﻿using AutoMapper;
using Dtos.ViewRequest;
using Service.Interface.IService;
using Service.Interface.IUnitOfWork;
using System.Threading.Tasks;

namespace Service.Service
{
    public class CommentDetailService : BaseService, ICommentDetailService
    {
        public CommentDetailService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            Mapper = mapper;
            UnitOfWork = unitOfWork;
        }
        public Task CreateCommentDetail(CommentDetailRq media)
        {
            throw new System.NotImplementedException();
        }

        public Task DeleteCommentDetail(int id)
        {
            throw new System.NotImplementedException();
        }

        public Task EditCommentDetail(CommentDetailRq post)
        {
            throw new System.NotImplementedException();
        }

    }
}
