﻿using AutoMapper;
using Common.Helper;
using Domain.Model;
using Dtos.ViewRequest;
using Service.Interface.IService;
using Service.Interface.IUnitOfWork;
using System;
using System.Threading.Tasks;

namespace Service.Service
{
    public class ReactionService: BaseService, IReactionService
    {
        private readonly IUserService UserHelper;
        public ReactionService(IMapper mapper, IUnitOfWork unitOfWork, IUserService userHelper)
        {
            Mapper = mapper;
            UnitOfWork = unitOfWork;
            UserHelper = userHelper;
        }

        public async Task ReactionPost(ReactionRq reactionRq)
        {
            Reaction reaction = Mapper.Map<Reaction>(reactionRq);
            reaction.ReactionTime = DateTime.Now;
            reaction.UserId = UserHelper.GetUserId();
            await UnitOfWork.ReactionRepository.Add(reaction);
            await UnitOfWork.CompleteAsync();
        }
    }
}
