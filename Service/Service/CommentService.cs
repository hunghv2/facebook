﻿using AutoMapper;
using Dapper;
using Domain.Model;
using Dtos.ViewModel;
using Dtos.ViewRequest;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Service.Interface.IService;
using Service.Interface.IUnitOfWork;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Service.Service
{
    public class CommentService : BaseService, ICommentService
    {
        private readonly IConfiguration _configuration;

        public CommentService(IMapper mapper, IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            Mapper = mapper;
            _configuration = configuration;
            UnitOfWork = unitOfWork;
        }
        public async Task CreateComment(CommentRq commentrq)
        {
            Comment comment = Mapper.Map<Comment>(commentrq);
            comment.CommentTime = DateTime.Now;
            await UnitOfWork.CommentRepository.Add(comment);
            await UnitOfWork.CompleteAsync();
        }

        public async Task DeleteComment(int id)
        {
            await UnitOfWork.PostRepository.Delete(id);
            await UnitOfWork.CompleteAsync();
        }

        public async Task EditComment(CommentRq commentrq)
        {
            Comment comment = Mapper.Map<Comment>(commentrq);
            await UnitOfWork.CommentRepository.Update(comment);
            await UnitOfWork.CompleteAsync();
        }


        public async Task<List<CommentVm>> GetCommentAsync(int id)
        {
            using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                if(conn.State == ConnectionState.Closed)
                {
                    await conn.OpenAsync();
                }
                var query = @$"WITH cte AS (
                        SELECT c.CommentId, c.CommentContent, c.ParentCommentId, c.CommentTime, 0 AS lvl
                            FROM dbo.Comments c
                            WHERE c.CommentId = {id}
                        UNION ALL
                        SELECT c.CommentId, c.CommentContent, c.ParentCommentId, c.CommentTime, cte.lvl + 1
                            FROM dbo.Comments c
                            JOIN cte on c.ParentCommentId = cte.CommentId
                    )
                    SELECT * FROM cte
                    ORDER BY lvl;";
                var result = await conn.QueryAsync<CommentVm>(query, null, null, 120, CommandType.Text);
                return result.ToList();
            }
        
        }


        public async Task<List<CommentVm>> GetChildComment(int id)
        {
            using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                if(conn.State == ConnectionState.Closed)
                {
                    await conn.OpenAsync();
                }
                var query = @$"SELECT c.CommentId, c.CommentContent, c.ParentCommentId, c.CommentTime
                            FROM dbo.Comments c
                            WHERE c.ParentCommentId = {id}";
                var result = await conn.QueryAsync<CommentVm>(query, null, null, 120, CommandType.Text);
                return result.ToList();
            }
        }

    }
}
